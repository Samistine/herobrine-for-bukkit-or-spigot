package org.jakub1221.herobrineai.hooks;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.jakub1221.customitems.API;
import org.jakub1221.customitems.CustomItems;

public class CustomItemsHook
{
  private CustomItems ci = null;
  private API api = null;
  
  public void init()
  {
    this.ci = ((CustomItems)Bukkit.getServer().getPluginManager().getPlugin("CustomItems"));
    this.api = CustomItems.getAPI();
  }
  
  public boolean Check()
  {
    return Bukkit.getServer().getPluginManager().getPlugin("CustomItems") != null;
  }
  
  public boolean checkItem(String name)
  {
    if (this.ci != null) {
      return this.api.itemExist(name);
    }
    return false;
  }
  
  public ItemStack getItem(String name)
  {
    return this.api.createItem(name);
  }
}
