package org.jakub1221.herobrineai.hooks;

import com.bekvon.bukkit.residence.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class ResidenceHook {

	public boolean Check() {
		return Bukkit.getServer().getPluginManager().getPlugin("Residence") != null;
	}

	public boolean isSecuredArea(final Location loc) {
		return Residence.getResidenceManager().getByLoc(loc) != null;
	}

}