package org.jakub1221.herobrineai.AI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jakub1221.herobrineai.HerobrineAI;
import org.jakub1221.herobrineai.Util;
import org.jakub1221.herobrineai.AI.cores.Attack;
import org.jakub1221.herobrineai.AI.cores.Book;
import org.jakub1221.herobrineai.AI.cores.BuildStuff;
import org.jakub1221.herobrineai.AI.cores.Burn;
import org.jakub1221.herobrineai.AI.cores.BuryPlayer;
import org.jakub1221.herobrineai.AI.cores.Curse;
import org.jakub1221.herobrineai.AI.cores.DestroyTorches;
import org.jakub1221.herobrineai.AI.cores.Graveyard;
import org.jakub1221.herobrineai.AI.cores.Haunt;
import org.jakub1221.herobrineai.AI.cores.Heads;
import org.jakub1221.herobrineai.AI.cores.Pyramid;
import org.jakub1221.herobrineai.AI.cores.RandomExplosion;
import org.jakub1221.herobrineai.AI.cores.RandomSound;
import org.jakub1221.herobrineai.AI.cores.Signs;
import org.jakub1221.herobrineai.AI.cores.SoundF;
import org.jakub1221.herobrineai.AI.cores.Temple;
import org.jakub1221.herobrineai.AI.cores.Totem;
import org.jakub1221.herobrineai.misc.ItemName;
import org.jakub1221.herobrineai.nms.entity.MobType;

public class AICore {

	public static final ConsoleLogger log = new ConsoleLogger();
	public static HerobrineAI plugin;
	public static Player PlayerTarget;
	public static boolean isTarget = false;
	public static int ticksToEnd = 0;
	public static boolean isDiscCalled = false;
	public static boolean isTotemCalled = false;
	public static int _ticks = 0;
	private ArrayList<Core> allCores = new ArrayList<Core>(Arrays.asList(new Core[] {
		new Attack(), new Book(), new BuildStuff(), new BuryPlayer(), new DestroyTorches(), new Graveyard(),
		new Haunt(), new Pyramid(), new Signs(), new SoundF(), new Totem(), new Heads(),
		new RandomSound(),  new RandomExplosion(), new Burn(), new Curse(), new Temple()
	}));
	private Core.CoreType currentCore = Core.CoreType.ANY;
	private ResetLimits resetLimits;
	private boolean BuildINT;
	private boolean MainINT;
	private boolean RandomCoreINT;
	private int MAIN_INT;
	private int BD_INT;
	private int RC_INT;

	public Core getCore(final Core.CoreType type) {
		for (final Core c : allCores) {
			if (c.getCoreType() == type) {
				return c;
			}
		}
		return null;
	}

	public AICore() {
		AICore.plugin = HerobrineAI.getPluginCore();
		resetLimits = new ResetLimits();
		AICore.log.info("[Herobrine] Herobrine is now running in debug mode.");
		findPlayer();
		startIntervals();
	}

	public Graveyard getGraveyard() {
		return (Graveyard) getCore(Core.CoreType.GRAVEYARD);
	}

	public void setCoreTypeNow(final Core.CoreType c) {
		currentCore = c;
	}

	public Core.CoreType getCoreTypeNow() {
		return currentCore;
	}

	public ResetLimits getResetLimits() {
		return resetLimits;
	}

	public void disableAll() {
		resetLimits.disable();
	}
	
	

	public void playerBedEnter(final Player player) {
		final int chance = new Random().nextInt(100);
		if (chance < 25) {
			graveyardTeleport(player);
		} else if (chance < 50) {
			setHauntTarget(player);
		} else if ((HerobrineAI.getPluginCore().getConfigDB().UseNPC_Demon) && (!HerobrineAI.isNPCDisabled)) {
		      HerobrineAI.getPluginCore().getEntityManager().spawnCustomSkeleton(player.getLocation(), MobType.DEMON);
	    }
	}

	public void findPlayer() {
		if ((!HerobrineAI.getPluginCore().getConfigDB().OnlyWalkingMode) && !AICore.isTarget) {
			final int att_chance = new Random().nextInt(100);
			log.info("[HerobrineAI] Generating find chance...");
			if (((att_chance - (HerobrineAI.getPluginCore().getConfigDB().ShowRate * 4)) < 55) && (Bukkit.getServer().getOnlinePlayers().size() > 0)) {
				log.info("[HerobrineAI] Finding target...");
				final Player[] allOnPlayers = Bukkit.getServer().getOnlinePlayers().toArray(new Player[0]);
				final int playerRolled = Util.getRandomPlayerNum(allOnPlayers);
				if (allOnPlayers[playerRolled].getEntityId() != HerobrineAI.herobrineEntityID) {
					if (HerobrineAI.getPluginCore().getConfigDB().useWorlds.contains(allOnPlayers[playerRolled].getLocation().getWorld().getName()) && HerobrineAI.getPluginCore().canAttackPlayerNoMSG(allOnPlayers[playerRolled])) {
						cancelTarget(Core.CoreType.ANY);
						AICore.PlayerTarget = allOnPlayers[playerRolled];
						AICore.isTarget = true;
						setCoreTypeNow(Core.CoreType.START);
						startAI();
					} else {
						log.info("[HerobrineAI] Target is in the safe world! (" + allOnPlayers[playerRolled].getLocation().getWorld().getName() + ")");
						findPlayer();
					}
				}
			}
		}
	}

	public void cancelTarget(final Core.CoreType coreType) {
		if ((coreType == currentCore) || (coreType == Core.CoreType.ANY)) {			
			if (AICore.isTarget) {
				if (currentCore == Core.CoreType.ATTACK) {
					((Attack) getCore(Core.CoreType.ATTACK)).StopHandler();
				}
				if (currentCore == Core.CoreType.HAUNT) {
					((Haunt) getCore(Core.CoreType.HAUNT)).StopHandler();
				}
				AICore._ticks = 0;
				AICore.isTarget = false;
				HerobrineAI.HerobrineHP = HerobrineAI.HerobrineMaxHP;
				AICore.log.info("[HerobrineAI] Cancelled teleporting of " + PlayerTarget.getDisplayName() + " to Herobrine's Graveyard.");
				final Location nowloc = new Location(Bukkit.getServer().getWorlds().get(0), 0.0, -20.0, 0.0);
				nowloc.setYaw(1.0f);
				nowloc.setPitch(1.0f);
				HerobrineAI.herobrineNPC.moveTo(nowloc);
				currentCore = Core.CoreType.ANY;
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(AICore.plugin, new Runnable() {
					@Override
					public void run() {
						AICore.this.findPlayer();
					}
				}, (6 / HerobrineAI.getPluginCore().getConfigDB().ShowRate) * (HerobrineAI.getPluginCore().getConfigDB().ShowInterval * 1L));
			}
		}
	}
	
	public void startAI() {
		if (AICore.PlayerTarget.isOnline() && AICore.isTarget) {
			if (!AICore.PlayerTarget.isDead()){
				final Object[] data = { AICore.PlayerTarget };
				final int chance = new Random().nextInt(100);
	            if (chance <= 10) {
	            	if (HerobrineAI.getPluginCore().getConfigDB().UseGraveyardWorld) {
						AICore.log.info("[Herobrine] Teleporting " + PlayerTarget.getDisplayName() + " to Herobrine's Graveyard.");
						getCore(Core.CoreType.GRAVEYARD).runCore(data);
					}				
				} else if (chance <= 25) {
			          getCore(Core.CoreType.ATTACK).runCore(data);
		        } else {
		          getCore(Core.CoreType.HAUNT).runCore(data);
		        }
			} else {
				cancelTarget(Core.CoreType.START);
			}
		} else {
			cancelTarget(Core.CoreType.START);
		}
	}

	public CoreResult setAttackTarget(final Player player) {
		final Object[] data = { player };
		return getCore(Core.CoreType.ATTACK).runCore(data);
	}

	public CoreResult setHauntTarget(final Player player) {
		final Object[] data = { player };
		return getCore(Core.CoreType.HAUNT).runCore(data);
	}

	public void graveyardTeleport(final Player player) {
		if (player.isOnline()) {
			cancelTarget(Core.CoreType.ANY);
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(AICore.plugin, new Runnable() {
				@Override
				public void run() {
					final Object[] data = { player };
					AICore.this.getCore(Core.CoreType.GRAVEYARD).runCore(data);
				}
			}, 10L);
		}
	}

	public void playerCallTotem(final Player player) {
		final String playername = player.getName();
		final Location loc = player.getLocation();
		AICore.isTotemCalled = true;
		cancelTarget(Core.CoreType.ANY);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(AICore.plugin, new Runnable() {
			@Override
			public void run() {
				AICore.this.cancelTarget(Core.CoreType.ANY);
				final Object[] data = { loc, playername };
				AICore.this.getCore(Core.CoreType.TOTEM).runCore(data);
			}
		}, 40L);
	}

	private void pyramidInterval() {
		if ((HerobrineAI.getPluginCore().getConfigDB().BuildPyramids) && new Random().nextBoolean() && (Bukkit.getServer().getOnlinePlayers().size() > 0)) {
			AICore.log.info("[Herobrine] Finding pyramid target...");
			final Player[] allOnPlayers = Bukkit.getServer().getOnlinePlayers().toArray(new Player[0]);
			final int playerRolled = Util.getRandomPlayerNum(allOnPlayers);
			if (HerobrineAI.getPluginCore().getConfigDB().useWorlds.contains(allOnPlayers[playerRolled].getLocation().getWorld().getName())) {
				final int chance2 = new Random().nextInt(100);
				if (chance2 < 30) {
					if (HerobrineAI.getPluginCore().getConfigDB().BuildPyramids) {
						final Object[] data = { allOnPlayers[playerRolled] };
						getCore(Core.CoreType.PYRAMID).runCore(data);
					}
				} else if (chance2 < 70) {
					if (HerobrineAI.getPluginCore().getConfigDB().BuryPlayers) {
						final Object[] data = { allOnPlayers[playerRolled] };
						getCore(Core.CoreType.BURY_PLAYER).runCore(data);
					}
				} else if (HerobrineAI.getPluginCore().getConfigDB().UseHeads) {
					final Object[] data = { allOnPlayers[playerRolled].getName() };
					getCore(Core.CoreType.HEADS).runCore(data);
				}
			}
		}
	}
	
	private void templeInterval() {
	    if ((HerobrineAI.getPluginCore().getConfigDB().BuildTemples) && (new Random().nextBoolean()) && (Bukkit.getServer().getOnlinePlayers().size() > 0)) {
	    	log.info("[HerobrineAI] Finding temple target...");
		      Player[] AllOnPlayers = Bukkit.getServer().getOnlinePlayers().toArray(new Player[0]);
		      int player_rolled = Util.getRandomPlayerNum(AllOnPlayers);
		      if ((HerobrineAI.getPluginCore().getConfigDB().useWorlds.contains(AllOnPlayers[player_rolled].getLocation().getWorld().getName()))) {
		        Object[] data = { AllOnPlayers[player_rolled] };
		        getCore(Core.CoreType.TEMPLE).runCore(data);
		      }
	      
	    }
	}

	private void buildCave() {
		if (HerobrineAI.getPluginCore().getConfigDB().BuildStuff && new Random().nextBoolean() && (Bukkit.getServer().getOnlinePlayers().size() > 0)) {
			final Player[] allOnPlayers = Bukkit.getServer().getOnlinePlayers().toArray(new Player[0]);
			final int playerRolled = Util.getRandomPlayerNum(allOnPlayers);
			if (HerobrineAI.getPluginCore().getConfigDB().useWorlds.contains(allOnPlayers[playerRolled].getLocation().getWorld().getName()) && new Random().nextBoolean()) {
				final Object[] data = { allOnPlayers[playerRolled].getLocation() };
				getCore(Core.CoreType.BUILD_STUFF).runCore(data);
			}
		}
	}

	public void callByDisc(final Player player) {
		AICore.isDiscCalled = false;
		if (player.isOnline()) {
			cancelTarget(Core.CoreType.ANY);
			setHauntTarget(player);
		}
	}

	public void randomCoreINT() {
		if (new Random().nextBoolean() && (Bukkit.getServer().getOnlinePlayers().size() > 0)) {
			final Player[] allOnPlayers = Bukkit.getServer().getOnlinePlayers().toArray(new Player[0]);
			final int playerRolled = Util.getRandomPlayerNum(allOnPlayers);
			if ((allOnPlayers[playerRolled].getEntityId() != HerobrineAI.herobrineEntityID)
					&& HerobrineAI.getPluginCore().getConfigDB().useWorlds.contains(allOnPlayers[playerRolled].getLocation().getWorld().getName())) {
				final Object[] data = { allOnPlayers[playerRolled] };
				if (HerobrineAI.getPluginCore().canAttackPlayerNoMSG(allOnPlayers[playerRolled])) {
					if (new Random().nextInt(100) < 30) {
						getCore(Core.CoreType.RANDOM_SOUND).runCore(data);
					} else if (new Random().nextInt(100) < 60) {
						if (HerobrineAI.getPluginCore().getConfigDB().Burn) {
							getCore(Core.CoreType.BURN).runCore(data);
						}
					} else if (new Random().nextInt(100) < 80) {
						if (HerobrineAI.getPluginCore().getConfigDB().Curse) {
							getCore(Core.CoreType.CURSE).runCore(data);
						}
					} else {
						getCore(Core.CoreType.RANDOM_EXPLOSION).runCore(data);
					}
				}
			}
		}
	}

	public void disappearEffect() {
		final Location ploc = AICore.PlayerTarget.getLocation();
		final Location hbloc1 = HerobrineAI.herobrineNPC.getBukkitEntity().getLocation();
		hbloc1.setY(hbloc1.getY() + 1.0);
		final Location hbloc2 = HerobrineAI.herobrineNPC.getBukkitEntity().getLocation();
		hbloc2.setY(hbloc2.getY() + 0.0);
		final Location hbloc3 = HerobrineAI.herobrineNPC.getBukkitEntity().getLocation();
		hbloc3.setY(hbloc3.getY() + 0.5);
		final Location hbloc4 = HerobrineAI.herobrineNPC.getBukkitEntity().getLocation();
		hbloc4.setY(hbloc4.getY() + 1.5);
		ploc.getWorld().playEffect(hbloc1, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc2, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc3, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc4, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc1, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc2, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc3, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc4, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc1, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc2, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc3, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc4, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc1, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc2, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc3, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc4, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc1, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc2, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc3, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc4, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc1, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc2, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc3, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc4, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc1, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc2, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc3, Effect.SMOKE, 80);
		ploc.getWorld().playEffect(hbloc4, Effect.SMOKE, 80);
		ploc.setY(-20.0);
		HerobrineAI.herobrineNPC.moveTo(ploc);
	}

	private void buildInterval() {
		if (new Random().nextInt(100) < 75) {
		      pyramidInterval();
		    } else {
		      templeInterval();
		    }
		    if (new Random().nextBoolean()) {
		      buildCave();
		    }
	}

	private void startIntervals() {
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(AICore.plugin, new Runnable() {
			@Override
			public void run() {
				AICore.this.startMAIN();
				AICore.this.startBD();
				AICore.this.startRC();
			}
		}, 5L);
	}

	public ItemStack createAncientSword() {
		ItemStack item = new ItemStack(Material.GOLD_SWORD);
	    String name = "Ancient Sword";
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add("AncientSword");
	    lore.add("Very old and mysterious sword.");
	    lore.add("This will protect you aganist Herobrine.");
	    item = ItemName.setNameAndLore(item, name, lore);
	    return item;
	}

	public void startBD() {
		BuildINT = true;
		BD_INT = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(AICore.plugin, new Runnable() {
			@Override
			public void run() {
				AICore.this.buildInterval();
			}
		}, 1L * HerobrineAI.getPluginCore().getConfigDB().BuildInterval, 1L * HerobrineAI.getPluginCore().getConfigDB().BuildInterval);
	}

	public void startMAIN() {
		MainINT = true;
		MAIN_INT = Bukkit
				.getServer()
				.getScheduler()
				.scheduleSyncRepeatingTask(AICore.plugin, new Runnable() {
					@Override
					public void run() {
						AICore.this.findPlayer();
					}
				}, (6 / HerobrineAI.getPluginCore().getConfigDB().ShowRate) * (HerobrineAI.getPluginCore().getConfigDB().ShowInterval * 1L),
						(6 / HerobrineAI.getPluginCore().getConfigDB().ShowRate) * (HerobrineAI.getPluginCore().getConfigDB().ShowInterval * 1L));
	}


	public void startRC() {
		RandomCoreINT = true;
		RC_INT = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(AICore.plugin, new Runnable() {
			@Override
			public void run() {
				AICore.this.randomCoreINT();
			}
		}, HerobrineAI.getPluginCore().getConfigDB().ShowInterval / 2L, HerobrineAI.getPluginCore().getConfigDB().ShowInterval / 2L);
	}

	public void stopBD() {
		if (BuildINT) {
			BuildINT = false;
			Bukkit.getServer().getScheduler().cancelTask(BD_INT);
		}
	}

	public void stopRC() {
		if (RandomCoreINT) {
			RandomCoreINT = false;
			Bukkit.getServer().getScheduler().cancelTask(RC_INT);
		}
	}

	public void stopMAIN() {
		if (MainINT) {
			MainINT = false;
			Bukkit.getServer().getScheduler().cancelTask(MAIN_INT);
		}
	}

}