package org.jakub1221.herobrineai.listeners;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.jakub1221.herobrineai.HerobrineAI;
import org.jakub1221.herobrineai.AI.AICore;
import org.jakub1221.herobrineai.AI.Core;
import org.jakub1221.herobrineai.misc.ItemName;
import org.jakub1221.herobrineai.nms.entity.MobType;

public class EntityListener implements Listener {

	  private ItemStack itemInHand;
	  private ArrayList<String> equalsLore;
	  private ArrayList<String> equalsLoreS;
	  private ArrayList<String> getLore;
	  
	@EventHandler
	public void onCreatureSpawn(final CreatureSpawnEvent event) {
		if (!HerobrineAI.isNPCDisabled && HerobrineAI.getPluginCore().getConfigDB().useWorlds.contains(event.getEntity().getLocation().getWorld().getName())) {
			final Entity entity = event.getEntity();
			final EntityType creatureType = event.getEntityType();
			if (event.isCancelled()) {
				return;
			}
			if (creatureType == EntityType.ZOMBIE) {
				if (HerobrineAI.getPluginCore().getConfigDB().UseNPC_Warrior && (new Random().nextInt(100) < HerobrineAI.getPluginCore().getConfigDB().npc.getInt("npc.Warrior.SpawnChance"))
						&& !HerobrineAI.getPluginCore().getEntityManager().isCustomMob(entity.getEntityId())) {
					final LivingEntity ent = (LivingEntity) entity;
					ent.setHealth(0);
					HerobrineAI.getPluginCore().getEntityManager().spawnCustomZombie(event.getLocation(), MobType.HEROBRINE_WARRIOR);
				}
			} else if ((creatureType == EntityType.SKELETON) && HerobrineAI.getPluginCore().getConfigDB().UseNPC_Demon
					&& (new Random().nextInt(100) < HerobrineAI.getPluginCore().getConfigDB().npc.getInt("npc.Demon.SpawnChance"))
					&& !HerobrineAI.getPluginCore().getEntityManager().isCustomMob(entity.getEntityId())) {
				final LivingEntity ent = (LivingEntity) entity;
				ent.setHealth(0);
				HerobrineAI.getPluginCore().getEntityManager().spawnCustomSkeleton(event.getLocation(), MobType.DEMON);
			}
		}
	}

	@EventHandler
	public void onEntityDeathEvent(final EntityDeathEvent event) {
		if (HerobrineAI.getPluginCore().getEntityManager().isCustomMob(event.getEntity().getEntityId())) {
			HerobrineAI.getPluginCore().getEntityManager().removeMob(event.getEntity().getEntityId());
		}
	}

	@EventHandler
	public void EntityTargetEvent(final EntityTargetLivingEntityEvent e) {
		final LivingEntity lv = e.getTarget();
		if (lv != null) {
			if (lv.getEntityId() == HerobrineAI.herobrineEntityID) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onEntityDamageByBlock(final EntityDamageByBlockEvent event) {
		if (event.getEntity().getEntityId() == HerobrineAI.herobrineEntityID) {
			event.setCancelled(true);
			event.setDamage(0);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEntityDamage(final EntityDamageEvent event) {		
		    if (event.getEntity().getEntityId() == HerobrineAI.herobrineEntityID)
		    {
		      if ((event instanceof EntityDamageByEntityEvent))
		      {
		        EntityDamageByEntityEvent dEvent = (EntityDamageByEntityEvent)event;
		        if ((HerobrineAI.getPluginCore().getConfigDB().Killable) && (HerobrineAI.getPluginCore().getAICore().getCoreTypeNow() != Core.CoreType.GRAVEYARD)) {
		          if ((dEvent.getDamager() instanceof Player))
		          {
		            if (event.getDamage() >= HerobrineAI.HerobrineHP)
		            {
		              int i = 1;
		              for (i = 1; i <= 2500; i++) {
		                if (HerobrineAI.getPluginCore().getConfigDB().config.contains("config.Drops." + Integer.toString(i)))
		                {
		                  Random randgen = new Random();
		                  int chance = randgen.nextInt(100);
		                  if (chance <= HerobrineAI.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".chance")) {
		                    HerobrineAI.herobrineNPC.getBukkitEntity().getLocation().getWorld().dropItemNaturally(HerobrineAI.herobrineNPC.getBukkitEntity().getLocation(), new ItemStack(Material.getMaterial(i), HerobrineAI.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".count")));
		                  }
		                }
		              }
		              HerobrineAI.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY);
		              HerobrineAI.HerobrineHP = HerobrineAI.HerobrineMaxHP;
		              Player player = (Player)dEvent.getDamager();
		              player.sendMessage("<Herobrine> " + HerobrineAI.getPluginCore().getConfigDB().DeathMessage);
		            }
		            else
		            {
		              HerobrineAI.HerobrineHP = (int)(HerobrineAI.HerobrineHP - event.getDamage());
		              HerobrineAI.herobrineNPC.hurtAnimation();
		              AICore.log.info("HIT: " + event.getDamage());
		            }
		          }
		          else if ((dEvent.getDamager() instanceof Projectile))
		          {
		            Arrow arrow = (Arrow)dEvent.getDamager();
		            if ((arrow.getShooter() instanceof Player))
		            {
		              if (HerobrineAI.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION)
		              {
		                HerobrineAI.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY);
		                HerobrineAI.getPluginCore().getAICore().setAttackTarget((Player)arrow.getShooter());
		              }
		              else if (event.getDamage() >= HerobrineAI.HerobrineHP)
		              {
		                int i = 1;
		                for (i = 1; i <= 2500; i++) {
		                  if (HerobrineAI.getPluginCore().getConfigDB().config.contains("config.Drops." + Integer.toString(i)))
		                  {
		                    Random randgen = new Random();
		                    int chance = randgen.nextInt(100);
		                    if (chance <= HerobrineAI.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".chance")) {
		                      HerobrineAI.herobrineNPC.getBukkitEntity().getLocation().getWorld().dropItemNaturally(HerobrineAI.herobrineNPC.getBukkitEntity().getLocation(), new ItemStack(Material.getMaterial(i), HerobrineAI.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".count")));
		                    }
		                  }
		                }
		                HerobrineAI.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY);
		                HerobrineAI.HerobrineHP = HerobrineAI.HerobrineMaxHP;
		                Player player = (Player)arrow.getShooter();
		                player.sendMessage("<Herobrine> " + HerobrineAI.getPluginCore().getConfigDB().DeathMessage);
		              }
		              else
		              {
		                HerobrineAI.HerobrineHP = (int)(HerobrineAI.HerobrineHP - event.getDamage());
		                HerobrineAI.herobrineNPC.hurtAnimation();
		                AICore.log.info("HIT: " + event.getDamage());
		              }
		            }
		            else if (HerobrineAI.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION)
		            {
		              Location newloc = HerobrineAI.herobrineNPC.getBukkitEntity().getLocation();
		              newloc.setY(-20.0D);
		              HerobrineAI.herobrineNPC.moveTo(newloc);
		              HerobrineAI.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY);
		            }
		          }
		          else if (HerobrineAI.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION)
		          {
		            Location newloc = HerobrineAI.herobrineNPC.getBukkitEntity().getLocation();
		            newloc.setY(-20.0D);
		            HerobrineAI.herobrineNPC.moveTo(newloc);
		            HerobrineAI.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY);
		          }
		        }
		      }
		      event.setCancelled(true);
		      event.setDamage(0);
		      return;
		    }
		    if ((event instanceof EntityDamageByEntityEvent))
		    {
		      EntityDamageByEntityEvent dEvent = (EntityDamageByEntityEvent)event;
		      if ((dEvent.getDamager() instanceof Player))
		      {
		        Player player = (Player)dEvent.getDamager();
		        if ((player.getItemInHand() != null) && (player.getItemInHand().getType() == Material.DIAMOND_SWORD) && (ItemName.getLore(player.getItemInHand()) != null))
		        {
		          this.itemInHand = player.getItemInHand();
		          this.getLore = ItemName.getLore(this.itemInHand);
		          if ((this.getLore.containsAll(this.equalsLoreS)) && (HerobrineAI.getPluginCore().getConfigDB().UseArtifactSword) && (new Random().nextBoolean())) {
		            player.getLocation().getWorld().strikeLightning(event.getEntity().getLocation());
		          }
		        }
		      }
		      else if ((dEvent.getDamager() instanceof Zombie))
		      {
		        Zombie zmb = (Zombie)dEvent.getDamager();
		        if ((zmb.getCustomName() == "Artifact Guardian") || (zmb.getCustomName() == "Herobrine´s Warrior")) {
		          dEvent.setDamage(dEvent.getDamage() * 3.0D);
		        }
		      }
		      else if ((dEvent.getDamager() instanceof Skeleton))
		      {
		        Skeleton zmb = (Skeleton)dEvent.getDamager();
		        if (zmb.getCustomName() == "Demon") {
		          dEvent.setDamage(dEvent.getDamage() * 3.0D);
		        }
		      }
		    }
		    if ((event.getCause() != null) && (event.getCause() == EntityDamageEvent.DamageCause.LIGHTNING) && ((event.getEntity() instanceof Player)) && (event.getEntity().getEntityId() != HerobrineAI.herobrineEntityID))
		    {
		      Player player = (Player)event.getEntity();
		      if ((player.getItemInHand() != null) && (player.getItemInHand().getType() == Material.DIAMOND_SWORD) && (ItemName.getLore(player.getItemInHand()) != null))
		      {
		        this.itemInHand = player.getItemInHand();
		        this.getLore = ItemName.getLore(this.itemInHand);
		        if ((this.getLore.containsAll(this.equalsLoreS)) && (HerobrineAI.getPluginCore().getConfigDB().UseArtifactSword))
		        {
		          event.setDamage(0);
		          event.setCancelled(true);
		          return;
		        }
		      }
		    }
		  
	}

	@EventHandler
	  public void onProjectileHit(ProjectileHitEvent event) {
	    if ((event.getEntity() instanceof Arrow)) {
	      Arrow arrow = (Arrow)event.getEntity();
	      if ((arrow.getShooter() instanceof Player)) {
	        Player player = (Player)arrow.getShooter();
	        if (player.getItemInHand() != null) {
	          this.itemInHand = player.getItemInHand();
	          if ((this.itemInHand.getType() != null) && (this.itemInHand.getType() == Material.BOW)) {
	            this.getLore = ItemName.getLore(this.itemInHand);
	            if ((this.getLore != null) && (this.getLore.containsAll(this.equalsLore)) && (HerobrineAI.getPluginCore().getConfigDB().UseArtifactBow)) {
	              player.teleport(arrow.getLocation());
	            }
	          }
	        }
	      }
	    }
	  }
}