package org.jakub1221.herobrineai.listeners;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.server.v1_8_R2.EntityPlayer;
import net.minecraft.server.v1_8_R2.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_8_R2.PacketPlayOutPlayerInfo;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Jukebox;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.jakub1221.herobrineai.HerobrineAI;
import org.jakub1221.herobrineai.AI.AICore;
import org.jakub1221.herobrineai.AI.Core;
import org.jakub1221.herobrineai.misc.ItemName;

public class PlayerListener implements Listener {

	  private ArrayList<String> equalsLoreS = new ArrayList<String>();
	  private ArrayList<String> equalsLoreA = new ArrayList<String>();
	  private ArrayList<LivingEntity> LivingEntities = new ArrayList<LivingEntity>();
	  private Location le_loc = null;
	  private Location p_loc = null;
	  private long timestamp = 0L;
	  private boolean canUse = false;
	  
	  public PlayerListener() {
	    this.equalsLoreS.add("Herobrine´s artifact");
	    this.equalsLoreS.add("Sword of Lighting");
	    this.equalsLoreA.add("Herobrine´s artifact");
	    this.equalsLoreA.add("Apple of Death");
	  }
	  
	@EventHandler
	public void onPlayerInteract(final PlayerInteractEvent event) {
		if (((event.getAction() == Action.LEFT_CLICK_BLOCK) || (event.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
			      (event.getClickedBlock() != null) && (event.getPlayer().getItemInHand() != null))
			    {
			      ItemStack itemInHand = event.getPlayer().getItemInHand();
			      if ((event.getPlayer().getItemInHand().getType() != null) && 
			        ((itemInHand.getType() == Material.DIAMOND_SWORD) || (itemInHand.getType() == Material.GOLDEN_APPLE)) && 
			        (ItemName.getLore(itemInHand) != null)) {
			        if ((ItemName.getLore(itemInHand).containsAll(this.equalsLoreS)) && (HerobrineAI.getPluginCore().getConfigDB().UseArtifactSword))
			        {
			          if (new Random().nextBoolean()) {
			            event.getPlayer().getLocation().getWorld().strikeLightning(event.getClickedBlock().getLocation());
			          }
			        }
			        else if ((ItemName.getLore(itemInHand).containsAll(this.equalsLoreA)) && (HerobrineAI.getPluginCore().getConfigDB().UseArtifactApple))
			        {
			          this.timestamp = (System.currentTimeMillis() / 1000L);
			          this.canUse = false;
			          if (HerobrineAI.getPluginCore().PlayerApple.containsKey(event.getPlayer()))
			          {
			            if (((Long)HerobrineAI.getPluginCore().PlayerApple.get(event.getPlayer())).longValue() < this.timestamp)
			            {
			              HerobrineAI.getPluginCore().PlayerApple.remove(event.getPlayer());
			              this.canUse = true;
			            }
			            else
			            {
			              this.canUse = false;
			            }
			          }
			          else {
			            this.canUse = true;
			          }
			          if (this.canUse)
			          {
			            event.getPlayer().getWorld().createExplosion(event.getPlayer().getLocation(), 0.0F);
			            this.LivingEntities = ((ArrayList<LivingEntity>)event.getPlayer().getLocation().getWorld().getLivingEntities());
			            HerobrineAI.getPluginCore().PlayerApple.put(event.getPlayer(), Long.valueOf(this.timestamp + 60L));
			            for (int i = 0; i <= this.LivingEntities.size() - 1; i++) {
			              if ((!(this.LivingEntities.get(i) instanceof Player)) && (this.LivingEntities.get(i).getEntityId() != HerobrineAI.herobrineEntityID))
			              {
			                this.le_loc = this.LivingEntities.get(i).getLocation();
			                this.p_loc = event.getPlayer().getLocation();
			                if ((this.le_loc.getBlockX() < this.p_loc.getBlockX() + 20) && (this.le_loc.getBlockX() > this.p_loc.getBlockX() - 20) && 
			                  (this.le_loc.getBlockY() < this.p_loc.getBlockY() + 10) && (this.le_loc.getBlockY() > this.p_loc.getBlockY() - 10) && 
			                  (this.le_loc.getBlockZ() < this.p_loc.getBlockZ() + 20) && (this.le_loc.getBlockZ() > this.p_loc.getBlockZ() - 20))
			                {
			                  event.getPlayer().getWorld().createExplosion(this.LivingEntities.get(i).getLocation(), 0.0F);
			                  this.LivingEntities.get(i).damage(10000);
			                }
			              }
			            }
			          }
			          else
			          {
			            event.getPlayer().sendMessage(ChatColor.RED + "Apple of Death is recharging!");
			          }
			        }
			      }
			    }
		
		
		if ((event.getClickedBlock() != null) && (event.getPlayer().getItemInHand() != null) && (event.getClickedBlock().getType() == Material.JUKEBOX)) {
			final ItemStack item = event.getPlayer().getItemInHand();
			final Jukebox block = (Jukebox) event.getClickedBlock().getState();
			if (!block.isPlaying() && (item.getType() == Material.RECORD_11)) {
				HerobrineAI.getPluginCore().getAICore();
				if (!AICore.isDiscCalled) {
					final Player player = event.getPlayer();
					HerobrineAI.getPluginCore().getAICore();
					AICore.isDiscCalled = true;
					HerobrineAI.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY);
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(AICore.plugin, new Runnable() {
						@Override
						public void run() {
							HerobrineAI.getPluginCore().getAICore().callByDisc(player);
						}
					}, 50L);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerEnterBed(final PlayerBedEnterEvent event) {
		if (new Random().nextInt(100) > 75) {
			final Player player = event.getPlayer();
			event.setCancelled(true);
			HerobrineAI.getPluginCore().getAICore().playerBedEnter(player);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerQuit(final PlayerQuitEvent event) {
		if (event.getPlayer().getEntityId() != HerobrineAI.herobrineEntityID) {
			HerobrineAI.getPluginCore().getAICore();
			if ((AICore.PlayerTarget == event.getPlayer()) && (HerobrineAI.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.GRAVEYARD) && (event.getPlayer().getLocation().getWorld() == Bukkit.getServer().getWorld(HerobrineAI.getPluginCore().getConfigDB().HerobrineWorldName))) {
				HerobrineAI.getPluginCore().getAICore();
				if (AICore.isTarget) {
					event.getPlayer().teleport(HerobrineAI.getPluginCore().getAICore().getGraveyard().getSavedLocation());
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerKick(final PlayerKickEvent event) {
		if (event.getPlayer().getEntityId() == HerobrineAI.herobrineEntityID) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerTeleport(final PlayerTeleportEvent event) {
		if (event.getPlayer().getEntityId() == HerobrineAI.herobrineEntityID) {
	      if (event.getFrom().getWorld() != event.getTo().getWorld()) {
	        HerobrineAI.getPluginCore().removeHerobrine();
	        HerobrineAI.getPluginCore().spawnHerobrine(event.getTo());
	        event.setCancelled(true);
	        return;
	      }
	      if ((HerobrineAI.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION) && 
	        (HerobrineAI.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockX() > HerobrineAI.getPluginCore().getConfigDB().WalkingModeXRadius) && 
	        (HerobrineAI.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockX() < -HerobrineAI.getPluginCore().getConfigDB().WalkingModeXRadius) && 
	        (HerobrineAI.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockZ() > HerobrineAI.getPluginCore().getConfigDB().WalkingModeZRadius) && 
	        (HerobrineAI.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockZ() < -HerobrineAI.getPluginCore().getConfigDB().WalkingModeZRadius)) {
	        HerobrineAI.getPluginCore().getAICore().cancelTarget(Core.CoreType.RANDOM_POSITION);
	        HerobrineAI.herobrineNPC.moveTo(new Location(Bukkit.getServer().getWorlds().get(0), 0.0D, -20.0D, 0.0D));
	      }
	    }
	}

	@EventHandler
	public void onPlayerCommand(final PlayerCommandPreprocessEvent event) {
		if (event.getPlayer().getWorld() == Bukkit.getServer().getWorld(HerobrineAI.getPluginCore().getConfigDB().HerobrineWorldName) && !event.getPlayer().hasPermission("hb-ai.cmdblockbypass")) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerDeathEvent(final PlayerDeathEvent event) {
		if (event.getEntity().getEntityId() == HerobrineAI.herobrineEntityID) {
			event.setDeathMessage("");
			HerobrineAI.getPluginCore().removeHerobrine();
			final Location nowloc = new Location(Bukkit.getServer().getWorlds().get(0), 0.0, -20.0, 0.0);
			nowloc.setYaw(1.0f);
			nowloc.setPitch(1.0f);
			HerobrineAI.getPluginCore().spawnHerobrine(nowloc);
		}
	}

	@EventHandler
	public void onPlayerMoveEvent(final PlayerMoveEvent event) {
		if ((event.getPlayer().getEntityId() != HerobrineAI.herobrineEntityID) && (event.getPlayer().getWorld() == Bukkit.getServer().getWorld(HerobrineAI.getPluginCore().getConfigDB().HerobrineWorldName))) {
			final Player player = event.getPlayer();
			player.teleport(new Location(Bukkit.getServer().getWorld(HerobrineAI.getPluginCore().getConfigDB().HerobrineWorldName), -2.49, 4.0, 10.69, -179.85f, 0.44999f));
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		EntityPlayer player = ((CraftPlayer) event.getPlayer()).getHandle();
		player.playerConnection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, HerobrineAI.herobrineNPC.getNMSEntity()));
	}

}