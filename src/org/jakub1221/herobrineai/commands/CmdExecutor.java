package org.jakub1221.herobrineai.commands;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jakub1221.herobrineai.HerobrineAI;
import org.jakub1221.herobrineai.AI.AICore;
import org.jakub1221.herobrineai.AI.Core;
import org.jakub1221.herobrineai.nms.NPC.entity.HumanNPC;

public class CmdExecutor implements CommandExecutor {

	private HerobrineAI P_Core;
	private Logger log;
	private ChatColor red = ChatColor.RED;

	public CmdExecutor(final HerobrineAI i) {
		super();
		P_Core = null;
		log = null;
		P_Core = i;
		log = HerobrineAI.log;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd,
			final String commandLabel, final String[] args) {
		if (sender instanceof Player) {
			final Player player = (Player) sender;
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("attack")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.attack")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkAttack(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										if (P_Core.canAttackPlayer(
												Bukkit.getServer().getPlayer(
														args[1]), player)) {
											if (!AICore.isTarget) {
												P_Core.getAICore()
														.setAttackTarget(
																Bukkit.getServer()
																		.getPlayer(
																				args[1]));
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is now attacking "
														+ args[1] + ".");
											} else {
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is already attacking "
														+ args[1]
														+ ". Use "
														+ ChatColor.GREEN
														+ "/herobrine cancel"
														+ ChatColor.RED
														+ " to stop the attack.");
											}
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] "
												+ args[1]
												+ " cannot be attacked because "
												+ "they are in a secure area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] "
											+ args[1]
											+ " cannot be attacked because they "
											+ "are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " cannot be attacked because they "
										+ "are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to initiate Herobrine "
									+ "attacks against other players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine attack <player name>");
					}
				} else if (args[0].equalsIgnoreCase("pyramid")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.pyramid")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkBuild(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										final Object[] data = { Bukkit
												.getServer().getPlayer(args[1]) };
										if (P_Core.getAICore()
												.getCore(Core.CoreType.PYRAMID)
												.runCore(data).getResult()) {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] Generating a pyramind near "
													+ args[1] + ".");
										} else {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] A pyramid could not be generated near "
													+ args[1]
													+ " because there is no good place for it near them.");
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] A pyramid could not be generated near "
												+ args[1]
												+ " because they are in a protected area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] A pyramid could not be generated near "
											+ args[1]
											+ " because they are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] A pyramid could not be generated near "
										+ args[1]
										+ " because they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to create Herobrine-"
									+ "built pyramids near players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine pyramid <player name>");
					}
				} else if (args[0].equalsIgnoreCase("temple")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.temple")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (this.P_Core.getSupport().checkBuild(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										final Object[] data = { Bukkit
												.getServer().getPlayer(args[1]) };
										if (P_Core.getAICore()
												.getCore(Core.CoreType.TEMPLE)
												.runCore(data).getResult()) {
											player.sendMessage(ChatColor.RED
													+ "[HerobrineAI] Creating temple near "
													+ args[1] + "!");
										} else {
											player.sendMessage(ChatColor.RED
													+ "[HerobrineAI] Cannot find good place for temple!");
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[HerobrineAI] Player is in secure area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[HerobrineAI] Player is offline.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[HerobrineAI] Player is offline.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to bury players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine temple <player name>");
					}
				} else if (args[0].equalsIgnoreCase("bury")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.bury")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkBuild(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										final Object[] data = { Bukkit
												.getServer().getPlayer(args[1]) };
										if (P_Core
												.getAICore()
												.getCore(
														Core.CoreType.BURY_PLAYER)
												.runCore(data).getResult()) {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] Herobrine has buried "
													+ args[1] + ".");
										} else {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] "
													+ args[1]
													+ " could not be buried "
													+ "because there is no good place to bury them.");
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] "
												+ args[1]
												+ " could not be buried "
												+ "because they are are in a protected area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] " + args[1]
											+ " could not be buried because "
											+ "they are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " could not be buried because "
										+ "they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to bury players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine bury <player name>");
					}
				} else if (args[0].equalsIgnoreCase("cave")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.cave")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = {
										Bukkit.getServer().getPlayer(args[1])
												.getLocation(), true };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(
														Core.CoreType.BUILD_STUFF)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] A cave could not be created near "
										+ args[1]
										+ " because they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to create caves near "
									+ "players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine cave <player name>");
					}
				} else if (args[0].equalsIgnoreCase("burn")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.burn")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = { Bukkit.getServer()
										.getPlayer(args[1]) };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(Core.CoreType.BURN)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " could not be burned because they "
										+ "not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to tell Herobrine "
									+ "to burn players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine burn <player name>");
					}
				} else if (args[0].equalsIgnoreCase("curse")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.curse")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = { Bukkit.getServer()
										.getPlayer(args[1]) };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(Core.CoreType.CURSE)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " cannot be cursed because they "
										+ "are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to tell Herobrine "
									+ "to curse players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine curse <player name>");
					}
				} else if (args[0].equalsIgnoreCase("heads")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.heads")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = { args[1] };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(Core.CoreType.HEADS)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ args[1]
										+ " cannot be haunted by heads because "
										+ "they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to haunt players "
									+ "with heads.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine heads <player name>");
					}
				} else if (args[0].equalsIgnoreCase("graveyard")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.graveyard")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (!AICore.isTarget) {
										P_Core.getAICore().graveyardTeleport(
												Bukkit.getServer().getPlayer(
														args[1]));
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] " + args[1]
												+ " has been teleported to "
												+ "Herobrine's Graveyard.");
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] Another player is already in Herobrine's "
												+ "Graveyard. Use "
												+ ChatColor.GREEN
												+ "/herobrine cancel"
												+ ChatColor.RED
												+ " to teleport the current player out of the graveyard.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] "
											+ args[1]
											+ " cannot be teleported to "
											+ "Herobrine's Graveyard because they are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ args[1]
										+ " cannot be teleported to "
										+ "Herobrine's Graveyard because they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the permissions necessary to teleport players "
									+ "to Herobrine's Graveyard.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine graveyard <player name>");
					}
				} else if (args[0].equalsIgnoreCase("haunt")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.haunt")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkHaunt(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										if (P_Core.canAttackPlayer(
												Bukkit.getServer().getPlayer(
														args[1]), player)) {
											if (!AICore.isTarget) {
												P_Core.getAICore()
														.setHauntTarget(
																Bukkit.getServer()
																		.getPlayer(
																				args[1]));
												String pname = args[1];
												Player target = Bukkit.getPlayer(pname);
												P_Core.herobrineNPC.lookAtPoint(target.getLocation());
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is now haunting "
														+ args[1] + ".");
											} else {
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is already haunting another player. Use "
														+ ChatColor.GREEN
														+ "/herobrine cancel"
														+ ChatColor.RED
														+ " to cancel the current haunting.");
											}
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] "
												+ args[1]
												+ " cannot be haunted because they"
												+ " are currently in a protected area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] "
											+ args[1]
											+ " cannot be haunted because they "
											+ "are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " cannot be haunted because they "
										+ "are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to initiate Herobrine "
									+ "hauntings.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine haunt <player name>");
					}
				} else if (args[0].equalsIgnoreCase("cancel")) {
					if (player.hasPermission("herobrine.cancel")) {
						P_Core.getAICore().cancelTarget(Core.CoreType.ANY);
						player.sendMessage(ChatColor.RED
								+ "[Herobrine] The current Herobrine victim has been saved.");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have the necessary permissions to cancel Herobrine's "
								+ "actions.");
					}
				} else if (args[0].equalsIgnoreCase("reload")) {
					if (player.hasPermission("herobrine.reload")) {
						P_Core.getConfigDB().Reload();
						player.sendMessage(ChatColor.RED
								+ "[Herobrine] Herobrine configuration file reloaded.");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have the necessary permissions to reload Herobrine's "
								+ "configuration file.");
					}
				} else if (args[0].equalsIgnoreCase("help")) {
					if (player.hasPermission("herobrine.help")) {
						player.sendMessage(ChatColor.RED
								+ "[Herobrine] Command List");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine help - Shows this list of Herobrine commands");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine attack <player name> - Sends Herobrine to attack the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine haunt <player name> - Sends Herobrine to haunt the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine cancel - Cancel's Herobrine's current actions against his current target");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine reload - Reloads the Herobrine configuration file");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine position - Displays the coordinates of Herobrine's current location");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine pyramid <player name> - Builds a pyramid near the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine bury <player name> - Buries the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine graveyard <player name> - Teleports the specified player to Herobrine's Graveyard");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine temple <player name> - Builds a temple near the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine heads <player name> - Spawns heads near the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine cave <player name> - Creates a cave near the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine burn <player name> - Burns the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine curse <player name> - Places Herobine's curse on the specified player");
						player.sendMessage(ChatColor.GREEN
								+ "/herobrine allworlds - Adds all server worlds to the list of worlds Herobrine is allowed in");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have the necessary permissions to view the Herobrine command documentation.");
					}
				} else if (args[0].equalsIgnoreCase("allworlds")) {
					if (player.hasPermission("herobrine.allworlds")) {
						HerobrineAI.getPluginCore().getConfigDB()
								.addAllWorlds();
						player.sendMessage(ChatColor.GREEN
								+ "[Herobrine] All server worlds have been added to the configuration file. Herobrine can now access all of the server's worlds.");
						player.sendMessage(ChatColor.YELLOW
								+ "[Herobrine] WARNING! - One or more server worlds was determined to have a space in its name. Please be aware that worlds with spaces in their name may cause problems.");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do no have the necessary permissions to add all server worlds to the configuration file.");
					}
				} else {
					player.sendMessage(ChatColor.RED + "Usage: /herobrine help");
				}
			} else {
				player.sendMessage(ChatColor.RED + "Usage: /herobrine help");
			}
			return true;
		}
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("attack")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkAttack(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								if (P_Core.canAttackPlayerConsole(Bukkit
										.getServer().getPlayer(args[1]))) {
									if (!AICore.isTarget) {
										P_Core.getAICore().setAttackTarget(
												Bukkit.getServer().getPlayer(
														args[1]));
										log.info("[Herobrine] Herobrine is now attacking "
												+ args[1] + ".");
									} else {
										log.info("[Herobrine] Herobrine is already attacking "
												+ args[1]
												+ ". Use /herobrine cancel to stop the attack.");
									}
								}
							} else {
								log.info("[Herobrine] "
										+ args[1]
										+ " cannot be attacked because they are in a secure area.");
							}
						} else {
							log.info("[Herobrine] "
									+ args[1]
									+ " cannot be attacked because they are not online.");
						}
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be attacked because they are not online.");
					}
				} else {
					log.info("Usage: /herobrine attack <player name>");
				}
			} else if (args[0].equalsIgnoreCase("pyramid")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkBuild(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								final Object[] data2 = { Bukkit.getServer()
										.getPlayer(args[1]) };
								if (P_Core.getAICore()
										.getCore(Core.CoreType.PYRAMID)
										.runCore(data2).getResult()) {
									log.info("[Herobrine] Generating a pyramind near "
											+ args[1] + ".");
								} else {
									log.info("[Herobrine] A pyramid could not be generated near "
											+ args[1]
											+ " because there is no good place for it near them.");
								}
							} else {
								log.info("[Herobrine] A pyramid could not be generated near "
										+ args[1]
										+ " because they are in"
										+ " a protected area.");
							}
						} else {
							log.info("[Herobrine] A pyramid could not be generated near "
									+ args[1] + " because they are not online.");
						}
					} else {
						log.info("[Herobrine] A pyramid could not be generated near "
								+ args[1] + " because they are not online.");
					}
				} else {
					log.info("Usage: " + ChatColor.GREEN
							+ "/herobrine pyramid <player name>");
				}
			} else if (args[0].equalsIgnoreCase("temple")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (this.P_Core.getSupport().checkBuild(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								final Object[] data = { Bukkit.getServer()
										.getPlayer(args[1]) };
								if (P_Core.getAICore()
										.getCore(Core.CoreType.TEMPLE)
										.runCore(data).getResult()) {
									log.info("[HerobrineAI] Creating temple near "
											+ args[1] + "!");
								} else {
									log.info("[HerobrineAI] Cannot find good place for temple!");
								}
							} else {
								log.info("[HerobrineAI] " + args[1]
										+ " is in secure area.");
							}
						} else {
							log.info("[HerobrineAI] " + args[1]
									+ " is offline.");
						}
					} else {
						log.info("[HerobrineAI] " + args[1] + " is offline.");
					}
				} else {
					log.info("Usage: /herobrine temple <player name>");
				}
			} else if (args[0].equalsIgnoreCase("bury")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkBuild(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								final Object[] data2 = { Bukkit.getServer()
										.getPlayer(args[1]) };
								if (P_Core.getAICore()
										.getCore(Core.CoreType.BURY_PLAYER)
										.runCore(data2).getResult()) {
									log.info("[Herobrine] Herobrine has buried "
											+ args[1] + ".");
								} else {
									log.info("[Herobrine] "
											+ args[1]
											+ " could not be buried "
											+ "because there is no good place to "
											+ "bury them.");
								}
							} else {
								log.info("[Herobrine] "
										+ args[1]
										+ " could not be buried "
										+ "because they are are in a protected area.");
							}
						} else {
							log.info("[Herobrine] " + args[1]
									+ " could not be buried because "
									+ "they are not online.");
						}
					} else {
						log.info("[Herobrine] " + args[1]
								+ " could not be buried because "
								+ "they are not online.");
					}
				} else {
					log.info("Usage: /herobrine bury <player name>");
				}
			} else if (args[0].equalsIgnoreCase("cave")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = {
								Bukkit.getServer().getPlayer(args[1])
										.getLocation(), true };
						log.info(ChatColor.RED
								+ "[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.BUILD_STUFF)
										.runCore(data2).getResultString());
					} else {
						log.info(ChatColor.RED
								+ "[Herobrine] A cave could not be created near "
								+ args[1] + " because they are not online.");
					}
				} else {
					log.info("Usage: /herobrine cave <player name>");
				}
			} else if (args[0].equalsIgnoreCase("burn")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = { Bukkit.getServer().getPlayer(
								args[1]) };
						log.info("[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.BURN)
										.runCore(data2).getResultString());
					} else {
						log.info("[Herobrine] " + args[1]
								+ " could not be burned because they "
								+ "not online.");
					}
				} else {
					log.info("Usage: /herobrine burn <player name>");
				}
			} else if (args[0].equalsIgnoreCase("curse")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = { Bukkit.getServer().getPlayer(
								args[1]) };
						log.info("[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.CURSE)
										.runCore(data2).getResultString());
					} else {
						log.info("[Herobrine] " + args[1]
								+ " cannot be cursed because they "
								+ "are not online.");
					}
				} else {
					log.info("Usage: /herobrine curse <player name>");
				}
			} else if (args[0].equalsIgnoreCase("heads")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = { args[1] };
						log.info("[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.HEADS)
										.runCore(data2).getResultString());
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be haunted by heads because they are not online.");
					}
				} else {
					log.info("Usage: " + ChatColor.GREEN
							+ "/herobrine heads <player name>");
				}
			} else if (args[0].equalsIgnoreCase("graveyard")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (!AICore.isTarget) {
								P_Core.getAICore().graveyardTeleport(
										Bukkit.getServer().getPlayer(args[1]));
								log.info("[Herobrine] " + args[1]
										+ " is now in the Graveyard world!");
							} else {
								log.info(ChatColor.RED
										+ "[Herobrine] Another player is already in Herobrine's "
										+ "Graveyard. Use "
										+ ChatColor.GREEN
										+ "/herobrine cancel"
										+ ChatColor.RED
										+ " to teleport the current player out of the graveyard.");
							}
						} else {
							log.info("[Herobrine] "
									+ args[1]
									+ " cannot be teleported to Herobrine's Graveyard because they are "
									+ "not online.");
						}
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be teleported to Herobrine's Graveyard because they are "
								+ "not online.");
					}
				} else {
					log.info("Usage: /herobrine graveyard <player name>");
				}
			} else if (args[0].equalsIgnoreCase("haunt")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkHaunt(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								if (P_Core.canAttackPlayerConsole(Bukkit
										.getServer().getPlayer(args[1]))) {
									if (!AICore.isTarget) {
										P_Core.getAICore().setHauntTarget(
												Bukkit.getServer().getPlayer(
														args[1]));
										log.info("[Herobrine] Herobrine is now haunting "
												+ args[1] + ".");
									} else {
										log.info("[Herobrine] Herobrine is already haunting another player. Use /herobrine cancel to"
												+ " cancel the current haunting.");
									}
								}
							} else {
								log.info("[Herobrine] "
										+ args[1]
										+ " cannot be haunted because they are currently in a protected area.");
							}
						} else {
							log.info("[Herobrine] "
									+ args[1]
									+ " cannot be haunted because they are not online.");
						}
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be haunted because they are not online.");
					}
				} else {
					log.info("Usage: /herobrine haunt <player name>");
				}
			} else if (args[0].equalsIgnoreCase("cancel")) {
				P_Core.getAICore().cancelTarget(Core.CoreType.ANY);
				log.info(ChatColor.RED
						+ "[Herobrine] The current Herobrine victim has been saved.");
			} else if (args[0].equalsIgnoreCase("reload")) {
				P_Core.getConfigDB().Reload();
				log.info("[Herobrine] Herobrine configuration file reloaded.");
			} else if (args[0].equalsIgnoreCase("position")) {
				// TODO: Position
				double x = P_Core.herobrineNPC.getNMSEntity().locX;
				double y = P_Core.herobrineNPC.getNMSEntity().locY;
				double z = P_Core.herobrineNPC.getNMSEntity().locZ;
				net.minecraft.server.v1_8_R2.World world = P_Core.herobrineNPC.getNMSEntity().getWorld();
				sender.sendMessage(red + "Herobrine's location world: " + red
						+ world + red + " x: " + red + x + red + " y: " + red
						+ y + red + " z: " + red + y);

			} else if (args[0].equalsIgnoreCase("help")) {
				log.info("[Herobrine] Command List");
				log.info("/herobrine help - Shows this list of Herobrine commands");
				log.info("/herobrine attack <player name> - Sends Herobrine to attack the specified player");
				log.info("/herobrine haunt <player name> - Sends Herobrine to haunt the specified player");
				log.info("/herobrine cancel - Cancel's Herobrine's current actions against his current target");
				log.info("/herobrine reload - Reloads the Herobrine configuration file");
				log.info("/herobrine position - Displays the coordinates of Herobrine's current location");
				log.info("/herobrine pyramid <player name> - Builds a pyramid near the specified player");
				log.info("/herobrine bury <player name> - Buries the specified player");
				log.info("/herobrine graveyard <player name> - Teleports the specified player to Herobrine's Graveyard");
				log.info("/herobrine temple <player name> - Builds a temple near the specified player");
				log.info("/herobrine heads <player name> - Spawns heads near the specified player");
				log.info("/herobrine cave <player name> - Creates a cave near the specified player");
				log.info("/herobrine burn <player name> - Burns the specified player");
				log.info("/herobrine curse <player name> - Places Herobine's curse on the specified player");
				log.info("/herobrine allworlds - Adds all server worlds to the list of worlds Herobrine is allowed in");
			}
			if (args[0].equalsIgnoreCase("allworlds")) {
				HerobrineAI.getPluginCore().getConfigDB().addAllWorlds();
				log.info("[Herobrine] All server worlds have been added to the configuration file. Herobrine can now access all of the server's worlds.");
				log.info("[Herobrine] WARNING! - One or more server worlds was determined to have a space in its name. Please be aware that worlds with spaces in their name may cause problems.");
			} else {
				log.info("Usage: /herobrine help");
			}
		} else {
			log.info("Usage: /herobrine help");
		}
		return true;
	}

}