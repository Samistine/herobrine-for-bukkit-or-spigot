package org.jakub1221.herobrineai;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.jakub1221.herobrineai.hooks.CustomItemsHook;
import org.jakub1221.herobrineai.hooks.FactionsHook;
import org.jakub1221.herobrineai.hooks.GriefPreventionHook;
import org.jakub1221.herobrineai.hooks.PreciousStonesHook;
import org.jakub1221.herobrineai.hooks.ResidenceHook;
import org.jakub1221.herobrineai.hooks.TownyHook;
import org.jakub1221.herobrineai.hooks.WorldGuardHook;

public class Support {
	private boolean B_Residence;
	private boolean B_GriefPrevention;
	private boolean B_Towny;
	private boolean B_WorldGuard;
	private boolean B_CustomItems = false;
	private boolean B_PreciousStones;
	private boolean B_Factions;
	private ResidenceHook ResidenceCore;
	private GriefPreventionHook GriefPreventionCore;
	private TownyHook TownyCore;
	private WorldGuardHook WorldGuard;
	private CustomItemsHook CustomItems = null;
	private PreciousStonesHook PreciousStones;
	private FactionsHook Factions;

	public Support() {
		ResidenceCore = new ResidenceHook();
		GriefPreventionCore = new GriefPreventionHook();
		TownyCore = new TownyHook();
	    CustomItems = new CustomItemsHook();
		WorldGuard = new WorldGuardHook();
		PreciousStones = new PreciousStonesHook();
		Factions = new FactionsHook();
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(HerobrineAI.getPluginCore(), new Runnable() {
			@Override
			public void run() {
				Support.this.CheckForPlugins();
			}
		}, 2L);
	}

	public boolean isPreciousStones() {
		return B_PreciousStones;
	}

	public boolean isWorldGuard() {
		return B_WorldGuard;
	}

	public boolean isResidence() {
		return B_Residence;
	}

	public boolean isGriefPrevention() {
		return B_GriefPrevention;
	}

	public boolean isTowny() {
		return B_Towny;
	}

	public boolean isFactions() {
		return B_Factions;
	}

	public void CheckForPlugins() {
		if (ResidenceCore.Check()) {
			B_Residence = true;
			HerobrineAI.log.info("[Herobrine] Residence plugin detected on server.");
		}
		if (GriefPreventionCore.Check()) {
			B_GriefPrevention = true;
			HerobrineAI.log.info("[Herobrine] GriefPrevention plugin detected on server.");
		}
		if (TownyCore.Check()) {
			B_Towny = true;
			HerobrineAI.log.info("[Herobrine] Towny plugin detected on server.");
		}
		if (WorldGuard.Check()) {
			B_WorldGuard = true;
			HerobrineAI.log.info("[Herobrine] WorldGuard plugin detected on server.");
		}
		if (PreciousStones.Check()) {
			B_PreciousStones = true;
			HerobrineAI.log.info("[Herobrine] PreciousStones plugin detected on server.");
		}
		if (Factions.Check()) {
			B_Factions = true;
			HerobrineAI.log.info("[Herobrine] Factions plugin detected on server.");
		}
		if (this.CustomItems.Check()) {
			this.B_CustomItems = true;
		    HerobrineAI.log.info("[HerobrineAI] CustomItems plugin detected!");
		    this.CustomItems.init();
	    }
	}

	public boolean isSecuredArea(final Location loc) {
		if (B_Residence) {
			return ResidenceCore.isSecuredArea(loc);
		}
		if (B_GriefPrevention) {
			return GriefPreventionCore.isSecuredArea(loc);
		}
		if (B_Towny) {
			return TownyCore.isSecuredArea(loc);
		}
		if (B_WorldGuard) {
			return WorldGuard.isSecuredArea(loc);
		}
		if (B_PreciousStones) {
			return PreciousStones.isSecuredArea(loc);
		}
		return B_Factions && Factions.isSecuredArea(loc);
	}

	public boolean checkBuild(final Location loc) {
		return HerobrineAI.getPluginCore().getConfigDB().SecuredArea_Build || !isSecuredArea(loc);
	}

	public boolean checkAttack(final Location loc) {
		return HerobrineAI.getPluginCore().getConfigDB().SecuredArea_Attack || !isSecuredArea(loc);
	}

	public boolean checkHaunt(final Location loc) {
		return HerobrineAI.getPluginCore().getConfigDB().SecuredArea_Haunt || !isSecuredArea(loc);
	}

	public boolean checkSigns(final Location loc) {
		return HerobrineAI.getPluginCore().getConfigDB().SecuredArea_Signs || !isSecuredArea(loc);
	}

	public boolean checkBooks(final Location loc) {
		return HerobrineAI.getPluginCore().getConfigDB().SecuredArea_Books || !isSecuredArea(loc);
	}
	
	public boolean isCustomItems() {
	    return this.B_CustomItems;
	  }
	
	public CustomItemsHook getCustomItems() {
	    if (this.B_CustomItems) {
	      return this.CustomItems;
	    }
	    return null;
	  }
}