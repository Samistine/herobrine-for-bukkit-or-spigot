# Herobrine for Bukkit or Spigot #

**The original Herobrine for Bukkit or Spigot repository is hosted by David-B at [https://bitbucket.org/David-B/herobrine-for-bukkit-or-spigot](https://bitbucket.org/David-B/herobrine-for-bukkit-or-spigot). If you wish to contribute to the project, please ensure that you make your fork of the repository from the original, and not from an existing fork.**

**Current Release Version: v1.1.1 (compiled from commit b27f427 for Bukkit/Spigot 1.8.3)**

**[Download latest version now.](https://bitbucket.org/David-B/herobrine-for-bukkit-or-spigot/downloads/Herobrine-1.1.1.jar)**

Herobrine for Bukkit or Spigot (or just "Herobrine" for short) is a derivative of the [Herobrine AI](http://dev.bukkit.org/bukkit-plugins/herobrine-ai/) plugin for Bukkit, originally written by Bukkit plugin developer [Jakub1221](http://dev.bukkit.org/profiles/Jakub1221/). This derivative is intended to maintain a rapid release cycle in an effort to patch bugs more frequently and also to try to update to the newest release of Bukkit/Spigot faster so that server administrators will be able to continue to have Herobrine stalk the players on their Minecraft server.
 
### What software license is this plugin distributed under? ###

As per the requirements set by the original developer, Herobrine for Bukkit or Spigot is distributed under the MIT License. The original copy of the MIT License that was distributed with Herobrine AI can be found [here](http://dev.bukkit.org/licenses/4-mit-license/).

### How do I contribute? ###

To contribute to the project, fork the Herobrine for Bukkit or Spigot repository, make whatever changes you desire to the code, and submit a pull request. I will review your changes and merge them if they are acceptable. Any changes, whether they are bug fixes or new features, are welcome.

In order to contribute, you will need to acquire Bukkit by making use of the [Spigot BuildTools](http://www.spigotmc.org/threads/buildtools-updates-information.42865/). You will also need to download the following Bukkit plugins and place them in a new folder named 'libs' within your copy of the Eclipse workspace in order to compile Herobrine. **If you have a fork of this repository that was created prior to the change in the way external libraries were handled, you may need to clean your project before the classpath changes take effect.**

* [CustomItems](http://dev.bukkit.org/bukkit-plugins/custom-items/)
* [Factions](http://dev.bukkit.org/bukkit-plugins/factions/)
* [GriefPrevention](http://dev.bukkit.org/bukkit-plugins/grief-prevention/)
* [MassiveCore](http://dev.bukkit.org/bukkit-plugins/mcore/)
* [PreciousStones](http://dev.bukkit.org/bukkit-plugins/preciousstones/)
* [Residence](http://dev.bukkit.org/bukkit-plugins/residence/)
* [Towny](http://palmergames.com/towny/)
* [WorldGuard](http://dev.bukkit.org/bukkit-plugins/worldguard/) 

### Contribution guidelines ###

Pull requests that do not provide detail on what changes were made to the code will be denied without a review. Please provide adequate information on the changes you made to the code. **If you are planning to submit a pull request for your changes, please use [Eclipse](https://eclipse.org/home/index.php) as your Java IDE to ensure that the file structure remains the same. If you use a different IDE, the file structure will be altered and I will not be able to easily merge your changes.**